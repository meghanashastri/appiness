package com.example.admin.appiness.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.admin.appiness.R;
import com.example.admin.appiness.models.DashboardModel;
import com.example.admin.appiness.utilities.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10-10-2017.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<DashboardModel> dashboardList, dbList;
    private List<DashboardModel> tempList;
    private DatabaseHelper databaseHelper;

    public DashboardAdapter(Context context) {
        this.context = context;
        this.dashboardList = new ArrayList<>();
        tempList = new ArrayList<>();
        dbList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(context);
    }

    public void getApiList(List<DashboardModel> apiList) {
        dbList = apiList;
    }

    public void updateData(List<DashboardModel> list) {
        this.dashboardList.addAll(list);
        this.tempList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.dashboard_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DashboardModel model = tempList.get(position);

        if (model != null) {
            if (model.getTitle() != null) {
                holder.tvTitle.setText(model.getTitle());
            }
            if (model.getNumberOfBackers() != null) {
                holder.tvBackers.setText(model.getNumberOfBackers());
            }
            if (model.getBy() != null) {
                holder.tvBy.setText("By: " + model.getBy());
            }
        }
    }

    @Override
    public int getItemCount() {
        return tempList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    tempList = dashboardList;
                } else {
                    ArrayList<DashboardModel> filteredList = new ArrayList<>();
                    for (DashboardModel dashboardModel : dbList) {
                        if (dashboardModel.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(dashboardModel);
                        }
                    }
                    tempList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempList = (ArrayList<DashboardModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvBy, tvBackers;
        private View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvBy = (TextView) itemView.findViewById(R.id.tvBy);
            tvBackers = (TextView) itemView.findViewById(R.id.tvBackers);
            mView = itemView;
        }

    }
}
