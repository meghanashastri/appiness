package com.example.admin.appiness.models;

        import com.google.gson.annotations.SerializedName;


/**
 * Created by Admin on 10-10-2017.
 */

public class DashboardModel {
    @SerializedName("s.no")
    private int sNo;
    private String by;
    @SerializedName("num.backers")
    private String numberOfBackers;
    private String title;

    public int getsNo() {
        return sNo;
    }

    public void setsNo(int sNo) {
        this.sNo = sNo;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getNumberOfBackers() {
        return numberOfBackers;
    }

    public void setNumberOfBackers(String numberOfBackers) {
        this.numberOfBackers = numberOfBackers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
