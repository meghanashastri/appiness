package com.example.admin.appiness.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.admin.appiness.models.DashboardModel;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 12-10-2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Appiness";
    private final String TABLE_NAME = "dashboardList";
    private final String COLUMN_S_NO = "sNo";
    private final String COLUMN_TITLE = "title";
    private final String COLUMN_NUM_OF_BACKERS = "numOfBackers";
    private final String COLUMN_BY = "by";
    private static final int DB_VERSION = 1;
    private Context mcontext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
        mcontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_S_NO + " INTEGER PRIMARY KEY, " + COLUMN_TITLE + " TEXT , "
                + COLUMN_NUM_OF_BACKERS + " TEXT , " + COLUMN_BY + " TEXT ) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //method to save data in the database
    public void saveListToDb(List<DashboardModel> list) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(" DELETE FROM " + TABLE_NAME);
        for (int i = 0; i < list.size(); i++) {
            values.put(COLUMN_S_NO, list.get(i).getsNo());
            values.put(COLUMN_TITLE, list.get(i).getTitle());
            values.put(COLUMN_NUM_OF_BACKERS, list.get(i).getNumberOfBackers());
            values.put(COLUMN_BY, list.get(i).getBy());
            db.insert(TABLE_NAME, null, values);
        }

        db.close();
    }


    //method to get list from database
    public List<DashboardModel> getAllDetails(int pageNo, int itemCount) {
        int startNum;
        startNum = ((pageNo - 1) * itemCount);
        List<DashboardModel> userDetailsList = new ArrayList<DashboardModel>();
        String getDetailsQuery = "SELECT * FROM "
                + " ( SELECT * FROM " + TABLE_NAME + " ORDER BY " + COLUMN_TITLE + " COLLATE NOCASE ) AS A " +
                " LIMIT " + itemCount + " OFFSET " + startNum;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(getDetailsQuery, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    DashboardModel userDetails = new DashboardModel();
                    userDetails.setsNo(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_S_NO))));
                    userDetails.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                    userDetails.setNumberOfBackers(cursor.getString(cursor.getColumnIndex(COLUMN_NUM_OF_BACKERS)));
                    userDetails.setBy(cursor.getString(cursor.getColumnIndex(COLUMN_BY)));
                    userDetailsList.add(userDetails);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDetailsList;
    }

}
