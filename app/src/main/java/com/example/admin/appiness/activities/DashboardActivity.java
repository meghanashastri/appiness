package com.example.admin.appiness.activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.admin.appiness.ApplicationLoader;
import com.example.admin.appiness.R;
import com.example.admin.appiness.adapters.DashboardAdapter;
import com.example.admin.appiness.models.DashboardModel;
import com.example.admin.appiness.utilities.CommonUtils;
import com.example.admin.appiness.utilities.Constants;
import com.example.admin.appiness.utilities.DatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    private DatabaseHelper databaseHelper;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loadingMore = false;
    private int pageNo = 1;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initRecyclerView();
        databaseHelper = new DatabaseHelper(this);
        //network check and api call
        if (CommonUtils.isConnectingToInternet(DashboardActivity.this)) {
            makeApiCall();
        } else {
            Toast.makeText(this, getResources().getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT).show();
        }

        dashboardPagination();
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.rvDashboard);
        adapter = new DashboardAdapter(DashboardActivity.this);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        progressLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);
    }

    /*
      Method to call api to get dashboard details
     */
    private void makeApiCall() {
        if (pageNo == 1) {
            progressLayout.setVisibility(View.VISIBLE);
            String url = Constants.DASHBOARD_URL;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("response", response.toString());
                            if (response != null && response.length() > 0) {
                                Type listType = new TypeToken<List<DashboardModel>>() {
                                }.getType();
                                List<DashboardModel> dashboardList = (List<DashboardModel>) new Gson().
                                        fromJson(response.toString(), listType);

                                //To sort the title in ascending order
                                Collections.sort(dashboardList, new Comparator<DashboardModel>() {
                                    @Override
                                    public int compare(DashboardModel lhs, DashboardModel rhs) {
                                        String title1 = lhs.getTitle().toUpperCase();
                                        String title2 = rhs.getTitle().toUpperCase();
                                        return title1.compareTo(title2);
                                    }
                                });
                                adapter.getApiList(dashboardList);

                                databaseHelper.saveListToDb(dashboardList);

                                adapter.updateData(databaseHelper.getAllDetails(pageNo, 20));

                                pageNo++;
                                loadingMore = false;
                                progressLayout.setVisibility(View.GONE);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingMore = false;
                    progressLayout.setVisibility(View.GONE);
                }
            });
            ApplicationLoader.getRequestQueue().add(jsonArrayRequest);
        } else {
            progressLayout.setVisibility(View.VISIBLE);
            adapter.updateData(databaseHelper.getAllDetails(pageNo, 20));
            pageNo++;
            loadingMore = false;
            progressLayout.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //method to implemet pagination
    private void dashboardPagination() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !(loadingMore)) {
                    if (CommonUtils.isConnectingToInternet(DashboardActivity.this)) {
                        loadingMore = true;
                        makeApiCall();
                    }
                }
            }
        });
    }
}
